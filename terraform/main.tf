variable "GCP_PROJECT" {}
variable "STATE_TF_BUCKET" {}

locals {
  bb = ["0.0.0.0/0"]
}

provider "google" {
  project = var.GCP_PROJECT
  region = "us-central1"
}

//Shared state
terraform {
  backend "gcs" {
    prefix = "terraform/state"
  }
}

resource "random_id" "db_name_suffix" {
  byte_length = 4
}

resource "random_password" "password" {
  length = 16
  special = true
  override_special = "_%@"
}

resource "google_sql_database_instance" "backend" {
  name             = "posup-${random_id.db_name_suffix.hex}"
  database_version = "POSTGRES_11"
  region           = "us-central1"
  deletion_protection  = false

  settings {
    tier = "db-f1-micro"
    ip_configuration {
      dynamic "authorized_networks" {
        for_each = local.bb
        iterator = bb
        content {
          value = "0.0.0.0/0"
        }
      }
    }
  }
}

resource "google_sql_user" "alunos-user" {
  name     = "alunos"
  instance = google_sql_database_instance.backend.name
  password = random_password.password.result
  depends_on = [
    google_sql_database_instance.backend
  ]
}

resource "google_sql_database" "alunos" {
  name     = "alunos"
  instance = google_sql_database_instance.backend.name
}

resource "null_resource" "db_setup" {
  triggers = {
     database = "{google_sql_database_instance.backend.public_ip_address}"
  }
  provisioner "local-exec" {
    command = "psql -h ${google_sql_database_instance.backend.public_ip_address} -p 5432 -U \"alunos\" -d alunos  -f \"../sql/initial.sql\""
    environment = {
      PGPASSWORD = "${random_password.password.result}"
    }
  }
  depends_on = [
    google_sql_database_instance.backend,
    google_sql_database.alunos,
    google_sql_user.alunos-user
  ]
}

resource "google_secret_manager_secret" "db_ip" {
  secret_id = "db_ip"

  replication {
    automatic = true
  }
}

resource "google_secret_manager_secret_version" "db_ip" {
  secret = google_secret_manager_secret.db_ip.id

  secret_data = "${google_sql_database_instance.backend.public_ip_address}"
}

resource "google_secret_manager_secret" "db_password" {
  secret_id = "db_password"

  replication {
    automatic = true
  }
}

resource "google_secret_manager_secret_version" "db_password" {
  secret = google_secret_manager_secret.db_password.id

  secret_data = "${random_password.password.result}"
}
