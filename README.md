# Backend

Este projeto trata-se do código da aplicação de Backend (Golang) implementada para a disciplina de Cloud Computing & DevSecOps do curso de pós graduação em engenharia Devops da Universidade Positivo em Curitiba/PR.

## Estrutura do projeto

* sql - Contém os scripts de inicialização de tabelas do banco de dados. Este script é utilizado pelo TerraForm para criação das tabelas no banco de dados.

* src - Diretório com código fonte da Aplicação

* terraform - Diretório com a automação de criação dos pré-requisitos na cloud para comportar uma aplicação de backend, provisionada como Google Cloud Function e seu banco de dados PostgreSQL

* bitbucket-pipelines.yml - Arquivo de configuração do pipeline

## Infra-Estrutura Cloud

Este projeto trata-se de uma aplicação de Backend básica, feita na linguagem Golang

Ele é provisionada no Google Cloud Platform, utilizando o recurso de **Google Cloud Functions**. Antes do seu deploy, um recurso do **Google Cloud SQL** é provisionado para comportar uma instância do banco PostgreSQL. O nome do database e a senha é gerado dinamicamente em tempo de pipeline e seus dados são armazenados no **Secret Manager** do Google.

## Pipeline

Estamos utilizando o Bitbucket Pipelines para provisionamento dos recursos de Cloud necessários, bem como o deploy do código da aplicação.

O provisionamento de componentes de infraestrutura está sendo feito via Terraform.

### Segurança

#### Secret Manager

O IP da instância do banco e a senha do database é gerada de forma randômica pelo terraform durante o pipeline. Esta senha é armazenada no serviço Secret Manager do Google e em nenhum momento é manipulada manualmente.

O processo de deploy da Function utiliza os dados armazenados no Secret Manager para embutir o database e a senha do banco em tempo de deploy.

#### Sonar

No processo de Pull Request e antes de cada solicitação de deploy para o ambiente executamos a análise estática de código através do Sonar Cloud. Só é permitido o merge do Pull Request, caso não seja relatado nenhuma infração através do Sonar.

Exemplo do retorno obtido no Pull Request:

Verificar exemplo no PR: https://bitbucket.org/geovanaSouza/backend/pull-requests/3

![Sonar](SonarPRBack.png)

### Deploy do ambiente

Mantemos o ambiente down por questões de economia $$$

Para efetuar o provisionamento e deploy da aplicação, seguir os seguintes passos:

* Acessar o menu pipelines: https://bitbucket.org/geovanaSouza/backend/addon/pipelines/home#!/

* Botão "Run pipeline" na tela superior direita

* Escolher branch: master e custom: deploy

![RunPipeline](RunPipelineDeploy.png)

* Pressionar o botão Run

* Primeiramente serão provisionados os recursos de Cloud Necessários para o ambiente (Banco de Dados, Armazenamento da Senha no Secret Manager, Inicialização das tabelas no banco). Esta etapa pode demorar de 10 à 15 minutos, pois este é o tempo médio de provisionamento de um banco de dados no GCP.

* Posteriormente será feito o deploy da aplicação na etapa "Deploy Backend"

* Após finalizado o pipeline, abrir o browser e acessar a URL https://us-central1-posup2020.cloudfunctions.net/Alunos

### Destroy ambiente

Para fins de economia é sempre aconselhável remover o ambiente após o uso. Para isto, existe um step no pipeline chamado "Destroy"

* Acessar o menu pipelines: https://bitbucket.org/geovanaSouza/backend/addon/pipelines/home#!/

* Botão "Run pipeline" na tela superior direita

* Escolher branch: master e custom: destroy

* Pressionar o botão Run

* Todos os componentes serão removidos da cloud

### Pré-Requisitos Pipeline

Em Deployments (https://bitbucket.org/geovanaSouza/backend/admin/addon/admin/pipelines/deployment-settings) existem as configurações de variáveis de ambiente necessárias para execução de cada um dos steps do pipeline.

#### Deployment prod-infrastructure

Definição de variáveis necessárias para execução do TerraForm. São elas:

* GOOGLE_CREDENTIALS - Credencial de cloud com permissão de provisionamento dos recursos que são pré-requisitos para o deploy da aplicação.

* STATE_TF_BUCKET - Nome do bucket onde será armazenado o arquivo de estado do TerraForm

* TF_VAR_FILE - Nome do arquivo que contém variáveis de ambiente necessárias para execução do TerraForm

#### Deployment prod-application

Definição de variáveis necessárias para execução do deploy do código fonte da aplicação. São elas:

* GCP_PROJECT - Projeto do Google Cloud Platform em que a aplicação será "deployada"

* KEY_FILE - Credencial de cloud com permissão para Deploy da aplicação

## Fluxo de desenvolvimento

* Crie uma feature branch à partir da master

* Efetue a alteração desejada

* Criar um Pull Request: origem: feature destino: master

* Obtenha uma aprovação do Default Reviewer. Atualmente Geovana e Victor são os aprovadores de modificações neste repositório. Este time recebe automaticamente e-mails de quando um Pull Request é efetuado para este repositório. Não é necessário notificá-los.

* Efetue o Merge do Pull Request.
