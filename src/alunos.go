package alunos

import (
	"database/sql"
	"fmt"
	"net/http"
	"os"

	_ "github.com/lib/pq"
)

func conectaComBancoDeDados() *sql.DB {
        
	dbPassword := os.Getenv("DB_PASSWORD")
	dbInstance := os.Getenv("DB_INSTANCE")

	conexao := "user=alunos dbname=alunos password=" + dbPassword + " host=" + dbInstance + " sslmode=disable"
	db, err := sql.Open("postgres", conexao)
	if err != nil {
		panic(err.Error())
	}
	return db
}

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}

func Alunos(w http.ResponseWriter, r *http.Request) {

	db := conectaComBancoDeDados()
	selectDeTodosOsAlunos, err := db.Query("select * from notas")
	if err != nil {
		panic(err.Error())
	}


	var retorno string

	retorno = "["

	for selectDeTodosOsAlunos.Next() {
		var id int
		var nome string
		var nota1 float32
		var nota2 float32

		err = selectDeTodosOsAlunos.Scan(&id, &nome, &nota1, &nota2)
		if err != nil {
			panic(err.Error())
		}

		nota1Str := fmt.Sprintf("%.2f", nota1)
		nota2Str := fmt.Sprintf("%.2f", nota2)

		retorno = retorno + "{ \"nome\": \"" + nome + "\", \"nota1\": \"" + nota1Str + "\", \"nota2\": \"" + nota2Str + "\"},"

	}

	if last := len(retorno) - 1; last >= 0 && retorno[last] == ',' {
		retorno = retorno[:last]
	}

	retorno = retorno + "]"

	enableCors(&w)

	fmt.Fprintf(w, retorno)

	defer db.Close()
}

