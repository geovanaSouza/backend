CREATE TABLE IF NOT EXISTS notas (
    nota_id INT GENERATED ALWAYS AS IDENTITY,
    aluno_nome VARCHAR(255),
    nota1 DECIMAL(3),
    nota2 DECIMAL(3),
    PRIMARY KEY(nota_id)
);

INSERT INTO notas(aluno_nome, nota1, nota2)
VALUES
    ('Adriano', 9, 7),
    ('Clayton', 9, 10),
    ('Emilio', 8, 10),
    ('Fernando', 9, 7),
    ('Geovana', 9, 9),
    ('Gustavo', 8, 9),
    ('Jeferson', 9, 8),
    ('João Victor', 7, 8),
    ('Jonathan', 8, 10),
    ('Juliano', 7, 10),
    ('Tiago', 8, 7),
    ('Victor', 10, 7);
